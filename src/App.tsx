import {BrowserRouter, Route} from 'react-router-dom';
import {MainPage} from "./routes/main/MainPage";
import {PlayerPage} from "./routes/player/PlayerPage";
import {ContentRoute} from "./routes/ContentRoute";
import {QueryClient, QueryClientProvider} from "react-query";

const queryClient = new QueryClient();

export function App() {
    return (
        <QueryClientProvider client={queryClient}>
            <BrowserRouter>
                <Route path="/player/:contentId" exact>
                    <ContentRoute>
                        <PlayerPage/>
                    </ContentRoute>
                </Route>
                <Route path="/" exact>
                    <MainPage/>
                </Route>
            </BrowserRouter>
        </QueryClientProvider>
    )
}

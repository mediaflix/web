import {createContext, useContext} from "react";
import {parseUrl} from "../util/parseUrl";

const ApiEndpointContext = createContext<URL>(
    parseUrl(localStorage.getItem("API_ENDPOINT"))
    || new URL("/", window.location.href)
);

export const ApiEndpointProvider = ApiEndpointContext.Provider;
export const ApiEndpointConsumer = ApiEndpointContext.Consumer;
export const useApiEndpoint = () => useContext<URL>(ApiEndpointContext);

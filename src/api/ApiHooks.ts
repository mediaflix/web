import {Content} from "./models/Content";
import {useQuery} from "react-query";
import {fetchJson} from "../util/fetchJson";
import {Instalment} from "./models/Instalment";
import {useApiEndpoint} from "./ApiEndpointContext";
import {ContentMeta} from "./models/dto/ContentMeta";
import {Genre} from "./models/Genre";

export function useShowEpisodes(
    item: Content | undefined
): [Instalment[], boolean, unknown] {
    const apiEndpoint = useApiEndpoint();

    const {data, isLoading, error} = useQuery(
        ["episodes", item?.ids?.uuid],
        () => item?.kind === "show"
            ? fetchJson<Instalment[]>(
                apiEndpoint,
                `api/v1/content/${item.ids.uuid}/episodes`,
                {
                    method: "GET"
                }
            )
            : []
    )

    return [data || [], isLoading, error];
}

export function useSingleContent(
    id: string
): [ContentMeta | null, boolean, unknown] {
    const apiEndpoint = useApiEndpoint();

    const {data, isLoading, error} = useQuery(
        ["media", id],
        () => fetchJson<ContentMeta>(
            apiEndpoint,
            `api/v1/content/${id}`,
            {
                method: "GET"
            }
        )
    )

    return [data || null, isLoading, error];
}

export function useAllContent(): [Content[], boolean, unknown] {
    const apiEndpoint = useApiEndpoint();

    const {data, isLoading, error} = useQuery(
        "media",
        () => fetchJson<Content[]>(
            apiEndpoint,
            `api/v1/content`,
            {
                method: "GET"
            }
        )
    );

    return [data || [], isLoading, error];
}

export function useGenres(): [Genre[], boolean, unknown] {
    const apiEndpoint = useApiEndpoint();

    const {data, isLoading, error} = useQuery(
        "media",
        () => fetchJson<Genre[]>(
            apiEndpoint,
            `api/v1/genres`,
            {
                method: "GET"
            }
        )
    );

    return [data || [], isLoading, error];
}

export function useGenreContent(genreId: string): [Content[], boolean, unknown] {
    const apiEndpoint = useApiEndpoint();

    const {data, isLoading, error} = useQuery(
        "media",
        () => fetchJson<Content[]>(
            apiEndpoint,
            `api/v1/genres/${genreId}`,
            {
                method: "GET"
            }
        )
    );

    return [data || [], isLoading, error];
}

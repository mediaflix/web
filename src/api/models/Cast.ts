import {Person} from "./Person";

export interface Cast {
    category: string | null,
    characters: string[],
    credit: string | null,
    person: Person
}

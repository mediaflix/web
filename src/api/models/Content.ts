import {Locale} from "../../util/locale/Locale";
import {LocalePriority} from "../../util/locale/LocalePriority";
import {selectLocaleVersion} from "../../util/locale/selectLocaleData";
import {selectRegionalVersion} from "../../util/locale/selectRegionalData";
import {PlayabilityRating} from "../../util/mime/PlayabilityRating";
import {selectPlayabileMedia} from "../../util/mime/usePlayabilityRating";
import {Cast} from "./Cast";
import {Genre} from "./Genre";
import {Image} from "./Image";
import {Media} from "./Media";
import {Rating} from "./Rating";
import {Subtitle} from "./Subtitle";
import {ContentDescription} from "./ContentDescription";
import {ContentId} from "./ContentId";
import {ContentKind} from "./ContentKind";
import {ContentName} from "./ContentName";

export interface Content {
    ids: ContentId,
    kind: ContentKind,
    originalLanguage: string | null,
    runtime: number | null,
    yearStart: number | null,
    yearEnd: number | null,
    titles: ContentName[],
    descriptions: ContentDescription[],
    cast: Cast[],
    genres: Genre[],
    ratings: Rating[],
    images: Image[],
    media: Media[],
    subtitles: Subtitle[],
    preview: string | null,
    createdAt: string,
    updatedAt: string
}

export const getLocalizedName = (content: Content, locale: Locale): ContentName | null =>
    selectLocaleVersion(locale, LocalePriority.REGION, content.titles)

export const getLocalizedDescription = (content: Content, locale: Locale): ContentDescription | null =>
    selectLocaleVersion(locale, LocalePriority.LOCALE, content.descriptions)

export const getLocalizedRating = (content: Content, locale: Locale): Rating | null =>
    selectRegionalVersion(locale, content.ratings)

export const getPlayableMedia = (content: Content, playabilityRating: (media: Media) => PlayabilityRating): Media | null =>
    selectPlayabileMedia(playabilityRating, content.media)

import {LocalizedData} from "../../util/locale/LocalizedData";

export interface ContentDescription extends LocalizedData {
    overview: string,
    tagline: string | null,
    languages: string[],
    kind: string
}

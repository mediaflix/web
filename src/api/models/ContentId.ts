export interface ContentId {
    uuid: string,
    imdb: string | null,
    tmdb: number | null,
    tvdb: number | null
}

export enum ContentKind {
    Movie = "movie",
    Show = "show",
    Episode = "episode"
}

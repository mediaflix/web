import {LocalizedData} from "../../util/locale/LocalizedData";

export interface ContentName extends LocalizedData {
    name: string,
    languages: string[],
    kind: string
}

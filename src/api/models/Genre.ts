export interface Genre {
    id: string,
    tmdbId: number | null,
    name: string
}

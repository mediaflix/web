export interface Image {
    kind: string,
    mime: string,
    language: string,
    src: string
}

export function findImage(
    images: Image[],
    type: string,
    languages: (string | null)[]
): Image | null {
    const imageList = images.filter(it => it.kind === type)

    for (let language of languages) {
        const image = imageList.find(it => it.language === language);
        if (image) {
            return image;
        }
    }

    if (languages.includes(null) && imageList.length) {
        return imageList[0];
    }

    return null;
}

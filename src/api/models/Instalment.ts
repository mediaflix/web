import {Content} from "./Content";

export interface Instalment {
    season: string | null,
    episode: string | null,
    airDate: string | null,
    content: Content
}

export function sortInstalments(instalments: Instalment[]): Instalment[] {
    return instalments.sort((a, b) => {
        if (a.season !== b.season) {
            if (a.season === null) return 1;
            if (b.season === null) return -1;
            return compareNumericStrings(a.season, b.season);
        } else if (a.episode !== b.episode) {
            if (a.episode === null) return 1;
            if (b.episode === null) return -1;
            return compareNumericStrings(a.episode, b.episode);
        } else {
            if (a.airDate === null) return 1;
            if (b.airDate === null) return -1;
            return compareDateStrings(a.airDate, b.airDate);
        }
    })
}

function compareDateStrings(a: string, b: string) {
    return new Date(a).getTime() - new Date(b).getTime();
}

function compareNumericStrings(a: string, b: string) {
    if (isNumeric(a) && isNumeric(b)) {
        return parseFloat(a) - parseFloat(b);
    } else {
        return a.localeCompare(b);
    }
}

function isNumeric(data: string): boolean {
    return !isNaN(data as any as number) && !isNaN(parseFloat(data))
}

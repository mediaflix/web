export interface Media {
    mime: string,
    codecs: string[],
    languages: string[],
    src: string
}

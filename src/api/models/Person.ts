export interface Person {
    id: string,
    imdbId: string | null,
    name: string
}

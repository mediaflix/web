import {LocalizedData} from "../../util/locale/LocalizedData";

export interface Rating extends LocalizedData {
    region: string | null,
    certification: string
}

export interface Subtitle {
    format: string,
    language: string | null,
    region: string | null,
    specifier: string | null,
    src: string
}

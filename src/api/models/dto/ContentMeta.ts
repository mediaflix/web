import {Instalment} from "../Instalment";
import {Content} from "../Content";

export interface ContentMeta {
    content: Content,
    instalment: Instalment | null,
}

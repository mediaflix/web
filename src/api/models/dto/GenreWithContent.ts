import {Genre} from "../Genre";
import {Content} from "../Content";

export interface GenreWithContent {
    genre: Genre,
    content: Content[],
}

import {produce} from 'immer'
import {StrictMode} from 'react';
import ReactDOM from 'react-dom';

import {defaults} from 'react-sweet-state';
import {App} from "./App";

import './assets/fonts/fira-sans.css';
import './assets/fonts/inter.css';
import './assets/fonts/tiresias.css';
import './assets/fonts/subtitles.css';
import './base/base.css';
import * as serviceWorker from './serviceWorker';

defaults.devtools = process.env.NODE_ENV !== "production";
defaults.mutator = (currentState, producer) => produce(currentState, producer)

ReactDOM.render(
    <StrictMode>
        <App/>
    </StrictMode>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

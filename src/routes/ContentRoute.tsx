import {PropsWithChildren} from "react";
import {useParams} from "react-router";
import {CurrentContentProvider} from "../util/CurrentContentContext";
import {useSingleContent} from "../api/ApiHooks";

export function ContentRoute(props: PropsWithChildren<{}>) {
    const {children} = props;
    const {contentId} = useParams<{ contentId: string }>();
    const [meta/*, isLoading, error*/] = useSingleContent(contentId);

    return (
        <CurrentContentProvider value={meta}>
            {children}
        </CurrentContentProvider>
    )
}

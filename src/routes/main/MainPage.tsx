import {Content, getLocalizedName} from "../../api/models/Content";
import {sortLexicallyAsc} from "../../util/sort/sortLexically";
import {MediaEntry} from "./MediaEntry";
import {LocaleProvider} from "../../util/locale/LocalizedContext";
import {useAllContent} from "../../api/ApiHooks";
import {Locale} from "../../util/locale/Locale";
import {useState} from "react";
import {createUseStyles} from "react-jss";

const locales: { [key: string]: Locale } = {
    "de-DE": {
        language: "de",
        region: "DE",
    },
    "de-AT": {
        language: "de",
        region: "AT",
    },
    "ja-JP": {
        language: "ja",
        region: "JP",
    },
    "en-US": {
        language: "en",
        region: "US",
    },
}

export function MainPage() {
    const classes = useStyles();
    const [locale, setLocale] = useState<Locale>(locales["en-US"]);
    const [media/*, isLoading, error*/] = useAllContent();

    return (
        <div>
            <select
                className={classes.languagePicker}
                value={locale.language + "-" + locale.region}
                onChange={(event) =>
                    setLocale(locales[event.target.value])}
            >
                {Object.keys(locales).map(code => (
                    <option key={code} value={code}>{code}</option>
                ))}
            </select>
            <LocaleProvider value={locale}>
                {media
                    .sort(sortLexicallyAsc(item => getLocalizedName(item, locale)?.name || ""))
                    .map((item: Content) => (
                        <MediaEntry
                            key={item.ids.uuid}
                            item={item}/>
                    ))}
            </LocaleProvider>
        </div>
    );
}

const useStyles = createUseStyles({
    languagePicker: {
        position: "fixed",
        right: 5,
        top: 5,
    },
});

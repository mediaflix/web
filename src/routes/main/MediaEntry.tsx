import {Content, getLocalizedDescription, getLocalizedName, getLocalizedRating} from "../../api/models/Content";
import {Fragment, useMemo} from "react";
import {Link} from "react-router-dom";
import {createUseStyles} from "react-jss";
import {useLocale} from "../../util/locale/LocalizedContext";
import {useShowEpisodes} from "../../api/ApiHooks";
import {MediaEpisode} from "./MediaEpisode";
import {findImage} from "../../api/models/Image";

export interface Props {
    item: Content
}

export function MediaEntry(
    {item}: Props
) {
    const locale = useLocale();
    const classes = useStyles();

    const [episodes/*, episodesLoading*/, episodesError] = useShowEpisodes(item);

    const title = getLocalizedName(item, locale);
    const description = getLocalizedDescription(item, locale);
    const rating = getLocalizedRating(item, locale);
    const [logo, poster, backdrop, still] = useMemo(() => [
        findImage(item.images, "logo", [locale.language]),
        findImage(item.images, "poster", [locale.language, item.originalLanguage, null]),
        findImage(item.images, "backdrop", [locale.language, item.originalLanguage, null]),
        findImage(item.images, "still", [null, locale.language, item.originalLanguage])
    ], [item.images, item.originalLanguage, locale.language]);

    return (
        <div className={classes.movie}>
            <h1>{title?.name}</h1>
            {logo && (
                <img
                    className={classes.poster}
                    alt={`Movie logo for ${title?.name}`}
                    src={logo.src}
                />
            )}
            <p><strong>{rating?.certification}</strong></p>
            <p><strong>{description?.tagline}</strong></p>
            <p>{description?.overview}</p>
            {item.kind === "movie" && (
                <p>
                    <Link to={"/player/" + item.ids.uuid}>Play</Link>
                </p>
            )}
            {item.kind === "show" && (
                <Fragment>
                    {episodesError ? (
                        <p>{"" + episodesError}</p>
                    ) : (
                        <ul>
                            {episodes.map(episode =>
                                <MediaEpisode
                                    key={episode.content.ids.uuid}
                                    item={episode}
                                />)}
                        </ul>
                    )}
                </Fragment>
            )}
            {poster && (
                <img
                    className={classes.poster}
                    alt={`Movie poster for ${title?.name}`}
                    src={poster.src}
                />
            )}
            {backdrop && (
                <img
                    className={classes.poster}
                    alt={`Movie backdrop for ${title?.name}`}
                    src={backdrop.src}
                />
            )}
            {still && (
                <img
                    className={classes.poster}
                    alt={`Movie still for ${title?.name}`}
                    src={still.src}
                />
            )}
        </div>
    )
}

const useStyles = createUseStyles({
    movie: {
        maxWidth: "40rem",
        margin: {
            left: "auto",
            right: "auto",
        }
    },
    poster: {
        maxWidth: "20rem",
        maxHeight: "20rem",
    }
});

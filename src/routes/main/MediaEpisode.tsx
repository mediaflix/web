import {Instalment} from "../../api/models/Instalment";
import {getLocalizedDescription, getLocalizedName} from "../../api/models/Content";
import {Link} from "react-router-dom";
import {useLocale} from "../../util/locale/LocalizedContext";
import {useMemo} from "react";
import {findImage} from "../../api/models/Image";
import {createUseStyles} from "react-jss";

export interface Props {
    item: Instalment,
    disabled?: boolean
}

export function MediaEpisode({item, disabled}: Props) {
    const classes = useStyles();
    const locale = useLocale();
    const episodeTitle = getLocalizedName(item.content, locale);
    const episodeDescription = getLocalizedDescription(item.content, locale);
    const [backdrop, still] = useMemo(() => [
        findImage(item.content.images, "backdrop", [locale.language, item.content.originalLanguage, null]),
        findImage(item.content.images, "still", [null, locale.language, item.content.originalLanguage])
    ], [item.content.images, item.content.originalLanguage, locale.language]);

    const episodeImage = still || backdrop;

    return (
        <li>
            <p>
                <strong>S{item.season}E{item.episode}</strong> – {item.airDate}
            </p>
            <p><strong>{episodeTitle?.name}</strong></p>
            <p><strong>{episodeDescription?.tagline}</strong></p>
            <p>{episodeDescription?.overview}</p>
            {episodeImage && (
                <img
                    className={classes.poster}
                    alt={episodeTitle?.name}
                    src={episodeImage.src}
                />
            )}
            {disabled ? (
                <p><u>Play</u></p>
            ) : (
                <p><Link to={"/player/" + item.content.ids.uuid}>Play</Link></p>
            )}
        </li>
    )
}

const useStyles = createUseStyles({
    poster: {
        maxWidth: "20rem",
        maxHeight: "20rem",
    }
});

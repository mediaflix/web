import {Fragment, useState} from "react";
import {createUseStyles} from "react-jss";
import {Link} from "react-router-dom";
import {ContentMeta} from "../../api/models/dto/ContentMeta";
import {Media} from "../../api/models/Media";
import {getLocalizedDescription, getLocalizedName, getLocalizedRating} from "../../api/models/Content";
import {useLocale} from "../../util/locale/LocalizedContext";
import {SeekBarContainer} from "./SeekBarContainer";
import {VideoElement} from "./video/VideoElement";
import {PlayerApi} from "./video/PlayerApi";
import {Subtitle} from "../../api/models/Subtitle";
import {MousePosition} from "../../util/mouse/MousePosition";
import {PlayerProvider} from "./video/VideoContext";
import {SubtitleRenderer} from "./subtitles/SubtitleRenderer";
import {useShowEpisodes} from "../../api/ApiHooks";
import {AudioSelection} from "./video/AudioSelection";
import {SubtitleSelection} from "./video/SubtitleSelection";
import {EpisodeSelection} from "./video/EpisodeSelection";
import {PlayerControls} from "./video/PlayerControls";

interface Props {
    meta: ContentMeta,
    media: Media,
}

export function Player(
    props: Props
) {
    console.log("Rendering Player")
    const {meta, media} = props;

    const {content, instalment} = meta;
    const classes = useStyles();
    const locale = useLocale();

    const [relatedEpisodes/*, relatedEpisodesLoading*/, relatedEpisodesError] = useShowEpisodes(instalment?.content);

    const name = getLocalizedName(content, locale);
    const description = getLocalizedDescription(content, locale);
    const rating = getLocalizedRating(content, locale);

    const [subtitle, setSubtitle] = useState<Subtitle | null>(null);

    const [playerApi, setPlayerApi] = useState<PlayerApi | null>(null);
    const [mousePosition, setMousePosition] = useState<MousePosition | null>(null);

    return (
        <PlayerProvider value={playerApi}>
            <div>
                <Link to="/">Back</Link>
                <h2>{name?.name}</h2>
                <p>{rating?.certification}</p>
                <strong>{description?.tagline}</strong>
                <p>{description?.overview}</p>
                {instalment?.content && (
                    <Fragment>
                        <h3>{getLocalizedName(instalment.content, locale)?.name}</h3>
                        {relatedEpisodesError ? (
                            <p>{"" + relatedEpisodesError}</p>
                        ) : (
                            <EpisodeSelection
                                content={content}
                                episodes={relatedEpisodes}
                            />
                        )}
                    </Fragment>
                )}
                <div className={classes.videoContainer}>
                    <div className={classes.videoCanvas}>
                        <VideoElement
                            className={classes.video}
                            media={media}
                            autoPlay={true}
                            ref={setPlayerApi}
                        />
                        <SubtitleRenderer
                            className={classes.subtitleCanvas}
                            subtitle={subtitle}
                        />
                    </div>
                </div>
                <PlayerControls/>
                <h3>Audio</h3>
                <AudioSelection/>
                <h3>Subtitles</h3>
                <SubtitleSelection
                    subtitles={[null, ...content.subtitles]}
                    subtitle={subtitle}
                    setSubtitle={setSubtitle}
                />
                <SeekBarContainer
                    previewSrc={content.preview}
                    mousePosition={mousePosition}
                    setMousePosition={setMousePosition}
                />
            </div>
        </PlayerProvider>
    );
}

const useStyles = createUseStyles({
    videoContainer: {
        width: "40rem",
        height: "30rem",
        background: "#dc5",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
    },
    videoCanvas: {
        position: "relative",
        display: "flex",
    },
    video: {
        maxWidth: "100%",
        maxHeight: "100%",
        minWidth: "100%",
        minHeight: "100%",
        margin: {
            left: "auto",
            right: "auto",
        },
    },
    subtitleCanvas: {
        position: "absolute",
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        width: "100%",
        height: "100%",
        textShadow: "#000 0px 0px 3px, rgba(0,0,0,0.5) 0px 0px 7px",
    },
});

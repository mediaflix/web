import {getPlayableMedia} from "../../api/models/Content";
import {usePlayabilityRating} from "../../util/mime/usePlayabilityRating";
import {useCurrentContent} from "../../util/CurrentContentContext";
import {Player} from "./Player";
import {PlayerError} from "./PlayerError";
import {PlayerLoading} from "./PlayerLoading";

export function PlayerPage() {
    console.log("Rendering player page");

    const content = useCurrentContent();
    const playabilityRating = usePlayabilityRating();

    if (content === null) {
        return (<PlayerLoading/>);
    }

    const media = getPlayableMedia(content.content, playabilityRating);
    if (media === null) {
        return (<PlayerError/>);
    }

    return (<Player meta={content} media={media}/>);
}

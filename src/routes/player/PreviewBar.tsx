import {useState} from "react";
import {createUseStyles} from "react-jss";
import {MousePosition} from "../../util/mouse/MousePosition";
import {useOffsetAbsoluteRef} from "../../util/offset/useOffsetAbsoluteRef";
import {PreviewViewer} from "./PreviewViewer";
import {Track} from "./video/Track";

interface Props {
    previewSrc: string | null,
    duration: number,
    position: MousePosition | null,
    hidden: boolean,
}

export function PreviewBar({previewSrc, duration, position, hidden}: Props) {
    const classes = useStyles();

    const [previewTrack, setPreviewTrack] = useState<HTMLTrackElement | null>(null);
    const [previewBarRef, previewHeadRef, offset] = useOffsetAbsoluteRef(position?.absolute || 0);

    return (
        <div
            ref={previewBarRef}
            className={classes.previewBar}
        >
            <div
                ref={previewHeadRef}
                className={classes.previewHead}
                style={{
                    transform: `translate3d(${offset}px, 0, 0)`,
                    opacity: hidden ? 0 : 1,
                }}
            >
                <Track
                    ref={setPreviewTrack}
                    kind="metadata"
                    label="previews"
                    src={previewSrc || undefined}
                />
                <PreviewViewer
                    previewTrack={previewTrack}
                    position={(position?.relative || 0) * duration}
                />
            </div>
        </div>
    );
}

const useStyles = createUseStyles({
    previewBar: {
        position: "relative",
        width: "40rem",
        display: "flex",
        flexDirection: "row",
        alignContent: "stretch",
        background: "#7c7",
    },
    previewHead: {
        maxWidth: "136rem",
        maxHeight: "9rem",
        display: "flex",
        flexDirection: "column-reverse",
        background: "#f00",
    }
});

import {Fragment, useMemo} from "react";
import {createUseStyles} from "react-jss";
import {HeadPortal} from "../../util/head/HeadPortal";
import {useImage} from "../../util/media/useImage";
import {useTextTrackCues} from "../../util/media/useTextTrackCues";
import {parseImageSprite} from "../../util/sprite/parseImageSprite";
import {useImageSprite} from "../../util/sprite/useImageSprite";

interface Props {
    previewTrack: HTMLTrackElement | null,
    position: number | null,
}

export function PreviewViewer({previewTrack, position}: Props) {
    const classes = useStyles();

    const cues = useTextTrackCues(previewTrack);
    const activeCue = position === null
        ? null
        : cues.find(it => it.startTime <= position && it.endTime >= position);
    const activeUrl = previewTrack && activeCue
        ? new URL(activeCue.text, previewTrack.src).toString()
        : null;
    const imageSprite = useMemo(() => parseImageSprite(activeUrl), [activeUrl]);
    const image = useImage(imageSprite?.src || null);
    const sprite = useImageSprite(imageSprite, image);
    const sources = useMemo(() => previewTrack === null
        ? null
        : Array.from(new Set(cues.map(it => {
            const url = new URL(it.text, previewTrack.src);
            url.hash = "";
            return url.toString();
        }))),
        [cues, previewTrack]
    );
    return useMemo(() => {
        return (
            <Fragment>
                <HeadPortal>
                    {sources?.map(it => (
                        <link key={it} rel="preload" href={it} as="image" crossOrigin="anonymous"/>
                    ))}
                </HeadPortal>
                <img alt="" className={classes.preview} src={sprite || undefined}/>
            </Fragment>
        )
    }, [classes.preview, sources, sprite]);
}

const useStyles = createUseStyles({
    preview: {
        maxWidth: "100%",
        maxHeight: "100%",
    }
});

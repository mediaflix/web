import {MouseEvent, useCallback, useMemo, useState} from "react";
import {createUseStyles} from "react-jss";
import {useOffsetAbsolute} from "../../util/offset/useOffsetAbsolute";
import {useOffsetRelative} from "../../util/offset/useOffsetRelative";
import {getMousePosition} from "../../util/mouse/getMousePosition";
import {MousePosition} from "../../util/mouse/MousePosition";
import {usePlayerApi} from "./video/VideoContext";
import {useCurrentTime} from "../../util/media/useCurrentTime";
import {useDuration} from "../../util/media/useDuration";

interface Props {
    mousePosition: MousePosition | null,
    setMousePosition: (position: MousePosition | null) => void,
}

export function SeekBar(
    {mousePosition, setMousePosition}: Props
) {
    const videoApi = usePlayerApi();
    const classes = useStyles();

    const isVisible = mousePosition !== null;

    const position = useCurrentTime(videoApi)
    const duration = useDuration(videoApi);

    const [seekBarRef, setSeekBarRef] = useState<HTMLDivElement | null>(null);
    const [seekHeadRef, setSeekHeadRef] = useState<HTMLDivElement | null>(null);
    const [playHeadRef, setPlayHeadRef] = useState<HTMLDivElement | null>(null);

    const seekHeadOffset = useOffsetAbsolute(seekBarRef, seekHeadRef, mousePosition?.absolute || 0);
    const playHeadOffset = useOffsetRelative(seekBarRef, playHeadRef, position / duration);

    const onMouseLeave = useCallback(() => {
        setMousePosition(null)
    }, [setMousePosition]);

    const onClick = useCallback((event: MouseEvent<HTMLDivElement>) => {
        const position = getMousePosition(event);
        setMousePosition(position);
        if (videoApi && position) {
            videoApi.setCurrentTime(position.relative * duration);
        }
    }, [duration, setMousePosition, videoApi]);

    const onMouseMove = useCallback((event: MouseEvent<HTMLDivElement>) => {
        const position = getMousePosition(event);
        const visible = position !== null;
        if (visible || isVisible !== visible) {
            setMousePosition(position);
        }
    }, [isVisible, setMousePosition]);

    const seekHead = useMemo(() => (
        <div
            ref={setSeekHeadRef}
            className={classes.seekHead}
            style={{
                opacity: isVisible ? 1 : 0,
                transform: `translate3d(${seekHeadOffset}px, 0, 0)`
            }}
        />
    ), [classes.seekHead, isVisible, seekHeadOffset]);

    const playHead = useMemo(() => (
        <div
            ref={setPlayHeadRef}
            className={classes.playHead}
            style={{
                transform: `translate3d(${playHeadOffset}px, 0, 0)`
            }}
        />
    ), [classes.playHead, playHeadOffset]);

    return (
        <div
            ref={setSeekBarRef}
            className={classes.seekBar}
            onMouseLeave={onMouseLeave}
            onClick={onClick}
            onMouseMove={onMouseMove}
        >
            {seekHead}
            {playHead}
        </div>
    );
}

const useStyles = createUseStyles({
    seekBar: {
        position: "relative",
        width: "40rem",
        display: "flex",
        flexDirection: "row",
        alignContent: "center",
        alignItems: "center",
        height: "5rem",
        background: "#77c",
    },
    seekHead: {
        width: "0.1rem",
        background: "#f00",
        position: "absolute",
        height: "1rem",
    },
    playHead: {
        width: "1rem",
        background: "#f00",
        position: "absolute",
        height: "1rem",
        borderRadius: "100%",
    }
});

import {Fragment} from "react";
import {MousePosition} from "../../util/mouse/MousePosition";
import {PreviewBar} from "./PreviewBar";
import {formatDuration} from "../../util/formatDuration";
import {useCurrentTime} from "../../util/media/useCurrentTime";
import {useDuration} from "../../util/media/useDuration";
import {usePlayerApi} from "./video/VideoContext";
import {SeekBar} from "./SeekBar";

interface Props {
    previewSrc: string | null,
    mousePosition: MousePosition | null,
    setMousePosition: (position: MousePosition | null) => void,
}

export function SeekBarContainer({previewSrc, mousePosition, setMousePosition}: Props) {
    const videoApi = usePlayerApi();

    const position = useCurrentTime(videoApi)
    const duration = useDuration(videoApi);

    return (
        <Fragment>
            <p style={{fontVariant: "tabular-nums"}}>
                {formatDuration(mousePosition
                    ? (mousePosition.relative * duration)
                    : position)} / {formatDuration(duration)}
            </p>
            <PreviewBar
                previewSrc={previewSrc}
                duration={duration}
                position={mousePosition}
                hidden={mousePosition === null}
            />
            <SeekBar
                mousePosition={mousePosition}
                setMousePosition={setMousePosition}
            />
        </Fragment>
    );
}

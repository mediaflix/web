import {useEffect, useState} from "react";
import {Subtitle} from "../../../api/models/Subtitle";
import SubtitlesOctopus from "../../../util/subtitles/ssa/SubtitlesOctopus";

interface Props {
    videoElement: HTMLVideoElement | null,
    subtitle: Subtitle | null,
    duration: number,
    className?: string,
}

export function SsaRenderer(
    {videoElement, subtitle, className}: Props
): JSX.Element {
    const [subtitleCanvas, setSubtitleCanvas] = useState<HTMLCanvasElement | null>(null);

    useEffect(() => {
        if (subtitleCanvas && videoElement && subtitle) {
            const instance = new SubtitlesOctopus({
                video: videoElement,
                canvas: subtitleCanvas,
                subUrl: subtitle?.src,
                workerUrl: '/assets/js/subtitles-octopus-worker.js',
                legacyWorkerUrl: '/assets/js/subtitles-octopus-worker-legacy.js',
                lossyRender: true,
                onError: (error: any) => {
                    console.log("Error rendering SSA subtitles:", error);
                },
                onReady: () => {
                    instance.setCurrentTime(videoElement?.currentTime);
                }
            })
            return () => {
                instance.dispose();
            }
        }
    }, [subtitleCanvas, subtitle, videoElement]);

    return (
        <canvas
            ref={setSubtitleCanvas}
            lang={subtitle?.language || undefined}
            className={className}
        />
    );
}

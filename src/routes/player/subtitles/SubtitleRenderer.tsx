import {Subtitle} from "../../../api/models/Subtitle";
import {Fragment} from "react";
import {TtmlRenderer} from "./TtmlRenderer";
import {SsaRenderer} from "./SsaRenderer";
import {usePlayerApi} from "../video/VideoContext";
import {useDuration} from "../../../util/media/useDuration";

interface Props {
    subtitle: Subtitle | null,
    className?: string,
}

export function SubtitleRenderer(
    props: Props
) {
    const playerApi = usePlayerApi();
    const videoElement = playerApi?.getVideoElement() || null;
    const duration = useDuration(playerApi);

    switch (props.subtitle?.format) {
        case "ttml":
            return (
                <TtmlRenderer
                    videoElement={videoElement}
                    duration={duration}
                    {...props}
                />
            );
        case "ass":
            return (
                <SsaRenderer
                    videoElement={videoElement}
                    duration={duration}
                    {...props}
                />
            );
        default:
            return (<Fragment/>);
    }
}

import {fromXML, generateISD, renderHTML, TtmlDocument} from "../../../util/subtitles/ttml/ismc";

interface TtmlState {
    target: HTMLElement | null,
    callback: number | null
    cache: any,
}

export default class TtmlHelper {
    static bindToTrack(track: HTMLTrackElement, target: HTMLElement, duration: number): () => void {
        console.debug(`Loading TTML track: ${track.src}`);

        const state: TtmlState = {
            target: target,
            callback: null,
            cache: null,
        }

        track.track.mode = "hidden";

        fetch(track.src)
            .then(response => response.text())
            .then(text => fromXML(text))
            .then(document => {
                if (document) {
                    const Cue = window.VTTCue || window.TextTrackCue;
                    const timeEvents = document.getMediaTimeEvents();
                    for (let i = 0; i < timeEvents.length; i++) {
                        let start = timeEvents[i];
                        let end = (i + 1 === timeEvents.length) ? duration : timeEvents[i + 1];

                        let cue = new Cue(start, end, "");
                        const callback = () => {
                            if (state.callback) {
                                window.cancelAnimationFrame(state.callback);
                            }
                            state.callback = window.requestAnimationFrame(() => {
                                TtmlHelper.renderTrack(track, document, state, start);
                            });
                        };
                        cue.addEventListener("enter", () => {
                            window.addEventListener("resize", callback);
                            callback();
                        });
                        cue.addEventListener("exit", () => {
                            if (state.callback) {
                                window.cancelAnimationFrame(state.callback);
                            }
                            state.callback = window.requestAnimationFrame(() => {
                                window.removeEventListener("resize", callback);
                                TtmlHelper.clearUi(state.target);
                            });
                        });
                        track.track.addCue(cue);
                    }
                }
            });

        return () => {
            this.clearUi(state.target);
            if (state.callback) {
                window.cancelAnimationFrame(state.callback);
            }
            state.target = null;
        }
    }

    static clearUi(target: HTMLElement | null) {
        if (target) {
            for (let child of Array.from(target.children)) {
                target.removeChild(child);
            }
        }
    }

    static renderTrack(track: HTMLTrackElement, document: TtmlDocument, state: TtmlState, time: number) {
        TtmlHelper.clearUi(state.target);
        if (track && state.target) {
            state.cache = renderHTML(
                generateISD(document, time),
                state.target,
                undefined,
                undefined,
                undefined,
                false,
                undefined,
                state.cache,
                undefined
            );
        }
    }
}

import {Fragment, useEffect, useState} from "react";
import TtmlHelper from "./TtmlHelper";
import {Subtitle} from "../../../api/models/Subtitle";
import {Track} from "../video/Track";

interface Props {
    videoElement: HTMLVideoElement | null,
    subtitle: Subtitle | null,
    duration: number,
    className?: string,
}

export function TtmlRenderer(
    {subtitle, duration, className}: Props
): JSX.Element {
    const [subtitleCanvas, setSubtitleCanvas] = useState<HTMLElement | null>(null);
    const [trackElement, setTrackElement] = useState<HTMLTrackElement | null>(null);

    useEffect(() => {
        if (subtitleCanvas && trackElement && subtitle) {
            return TtmlHelper.bindToTrack(trackElement, subtitleCanvas, duration)
        }
    }, [subtitleCanvas, subtitle, trackElement, duration]);

    return (
        <Fragment>
            {subtitle && (
                <Track
                    ref={setTrackElement}
                    kind="captions"
                    label={`${subtitle.language} (${subtitle.specifier})`}
                    src={subtitle.src}
                />
            )}
            <div
                ref={setSubtitleCanvas}
                lang={subtitle?.language || undefined}
                className={className}
            />
        </Fragment>
    );
}

import {useAudioTracks} from "../../../util/media/useAudioTracks";
import {usePlayerApi} from "./VideoContext";

export function AudioSelection() {
    const playerApi = usePlayerApi();

    const [audioTracks, currentTrack, setCurrentTrack] = useAudioTracks(playerApi);

    return (
        <ul>
            {audioTracks.map(track => (
                <li key={track.index}>
                    <strong>{track.lang}</strong>
                    &nbsp;{track.labels}
                    &nbsp;
                    <button
                        disabled={currentTrack === track}
                        onClick={() => setCurrentTrack(track)}
                    >
                        Choose
                    </button>
                </li>
            ))}
        </ul>
    );
}

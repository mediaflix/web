import {forwardRef, PropsWithChildren, useEffect, useImperativeHandle, useRef, useState,} from "react";
import {Media} from "../../../api/models/Media";
import dashjs, {MediaInfo} from "dashjs";
import {PlayerApi} from "./PlayerApi";

interface Props {
    media: Media,
    autoPlay: boolean,
    className?: string,
}

export const DashVideoElement = forwardRef<PlayerApi, PropsWithChildren<Props>>(function (
    {media, autoPlay, className, children},
    ref
) {
    const playerContainer = useRef<dashjs.MediaPlayerClass>(dashjs.MediaPlayer().create());
    const player = playerContainer.current;
    const [videoElement, setVideoElement] = useState<HTMLVideoElement | null>(null);

    useImperativeHandle(ref, () => ({
        DURATIONCHANGE_EVENT: dashjs.MediaPlayer.events.PLAYBACK_METADATA_LOADED,
        CANPLAY_EVENT: dashjs.MediaPlayer.events.PLAYBACK_METADATA_LOADED,
        TIMECHANGE_EVENT: dashjs.MediaPlayer.events.PLAYBACK_TIME_UPDATED,
        PLAY_EVENT: dashjs.MediaPlayer.events.PLAYBACK_PLAYING,
        PAUSE_EVENT: dashjs.MediaPlayer.events.PLAYBACK_PAUSED,
        play(): void {
            if (!player.isReady()) {
                return;
            }
            player.play();
        },
        pause(): void {
            if (!player.isReady()) {
                return;
            }
            player.pause();
        },
        isPaused(): boolean {
            if (!player.isReady()) {
                return true;
            }
            return player.isPaused();
        },
        setPlaybackRate(value: number): void {
            if (!player.isReady()) {
                return;
            }
            player.setPlaybackRate(value);
        },
        getPlaybackRate(): number {
            if (!player.isReady()) {
                return 1;
            }
            return player.getPlaybackRate();
        },
        setCurrentTime(value: number): void {
            if (!player.isReady()) {
                return;
            }
            player.seek(value);
        },
        setVolume(value: number): void {
            if (!videoElement) {
                return;
            }

            videoElement.volume = value;
        },
        getVolume(): number {
            if (!videoElement) {
                return 0;
            }

            return videoElement.volume;
        },
        getCurrentTime(): number {
            if (!player.isReady()) {
                return 0;
            }
            return player.time();
        },
        getDuration(): number {
            if (!player.isReady()) {
                return 0;
            }
            return player.duration();
        },
        canPlay(): boolean {
            if (!player.isReady()) {
                return false;
            }
            return !Number.isNaN(player.duration());
        },
        getAudioTracks(): MediaInfo[] {
            if (!player.isReady()) {
                return [];
            }
            return player.getTracksFor("audio").map((info: MediaInfo) => {
                return info;
            })
        },
        getCurrentAudioTrack(): MediaInfo | null {
            if (!player.isReady()) {
                return null;
            }

            return player.getCurrentTrackFor("audio");
        },
        setCurrentAudioTrack(track: MediaInfo): void {
            if (!player.isReady()) {
                return;
            }
            if (this.getCurrentAudioTrack()?.representationCount !== track.representationCount) {
                player.setQualityFor("audio", 0);
            }
            player.setCurrentTrack(track);
        },
        addEventListener(event: string, listener: () => void): void {
            player.on(event, listener);
        },
        removeEventListener(event: string, listener: () => void): void {
            player.off(event, listener);
        },
        debug(): any {
            return player;
        },
        getVideoElement(): HTMLVideoElement | null {
            return videoElement;
        }
    }), [player, videoElement]);

    useEffect(() => {
        player.initialize();
        player.setTrackSwitchModeFor("audio", "alwaysReplace");
        return () => {
            return player.reset();
        }
    }, [player]);

    useEffect(() => {
        if (videoElement) {
            player.attachView(videoElement)
        }
        return () => {
            // @ts-ignore
            player.attachView(null);
        }
    }, [player, videoElement]);

    useEffect(() => {
        player.attachSource(media.src);
        return () => {
            // @ts-ignore
            player.attachSource(null);
        }
    }, [player, media]);

    useEffect(() => {
        player.setAutoPlay(autoPlay);
    }, [player, autoPlay]);

    return (
        <video
            ref={setVideoElement}
            className={className}
            crossOrigin="anonymous"
            controls={false}
        >
            {children}
        </video>
    );
});

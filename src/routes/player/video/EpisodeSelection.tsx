import {MediaEpisode} from "../../main/MediaEpisode";
import {Instalment} from "../../../api/models/Instalment";
import {Content} from "../../../api/models/Content";

interface Props {
    content: Content,
    episodes: Instalment[],
}

export function EpisodeSelection(
    {episodes, content}: Props
) {
    return (
        <ul>
            {episodes.map(episode =>
                <MediaEpisode
                    key={episode.content.ids.uuid}
                    item={episode}
                    disabled={episode.content.ids.uuid === content.ids.uuid}
                />
            )}
        </ul>
    );
}

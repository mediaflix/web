import {MediaInfo} from "dashjs";

export interface PlayerApi {
    DURATIONCHANGE_EVENT: string
    CANPLAY_EVENT: string
    TIMECHANGE_EVENT: string
    PLAY_EVENT: string
    PAUSE_EVENT: string

    play(): void

    pause(): void

    isPaused(): boolean

    setPlaybackRate(value: number): void

    getPlaybackRate(): number

    setCurrentTime(value: number): void

    getCurrentTime(): number

    getDuration(): number

    setVolume(value: number): void

    getVolume(): number

    canPlay(): boolean

    getAudioTracks(): MediaInfo[]

    getCurrentAudioTrack(): MediaInfo | null

    setCurrentAudioTrack(track: MediaInfo): void

    addEventListener(event: string, listener: () => void): void

    removeEventListener(event: string, listener: () => void): void

    debug(): any

    getVideoElement(): HTMLVideoElement | null

}

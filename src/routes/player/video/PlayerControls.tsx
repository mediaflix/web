import {ChangeEvent, Fragment, useCallback, useState} from "react";
import {usePaused} from "../../../util/media/usePaused";
import {usePlayerApi} from "./VideoContext";

export function PlayerControls() {
    const playerApi = usePlayerApi();

    const [volume, setVolume] = useState<number>(1);
    const paused = usePaused(playerApi);

    const onPause = useCallback(() => {
        if (playerApi) {
            playerApi.pause()
        }
    }, [playerApi]);

    const onPlay = useCallback(() => {
        if (playerApi) {
            playerApi.play()
        }
    }, [playerApi]);

    const onFastForward = useCallback(() => {
        if (playerApi) {
            playerApi.setCurrentTime(playerApi.getCurrentTime() + 10)
        }
    }, [playerApi]);

    const onRewind = useCallback(() => {
        if (playerApi) {
            playerApi.setCurrentTime(playerApi.getCurrentTime() - 10)
        }
    }, [playerApi]);

    return (
        <Fragment>
            <button
                onClick={onPlay}
                disabled={!paused}
            >
                Play
            </button>
            <button
                onClick={onPause}
                disabled={paused}
            >
                Pause
            </button>
            <button
                onClick={onRewind}
            >
                Rewind
            </button>
            <button
                onClick={onFastForward}
            >
                Fast Forward
            </button>
            <p>
                <input
                    type="range"
                    min="0"
                    max="100"
                    value={volume * 100}
                    onChange={(event: ChangeEvent<HTMLInputElement>) => {
                        setVolume(event.target.valueAsNumber / 100);
                        playerApi?.setVolume(event.target.valueAsNumber / 100)
                    }}
                />
            </p>
        </Fragment>
    );
}

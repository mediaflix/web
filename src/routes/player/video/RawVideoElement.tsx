import {forwardRef, PropsWithChildren, useImperativeHandle, useState} from "react";
import {Media} from "../../../api/models/Media";
import {PlayerApi} from "./PlayerApi";
import {MediaInfo} from "dashjs";

interface Props {
    media: Media,
    autoPlay: boolean,
    className?: string,
}

export const RawVideoElement = forwardRef<PlayerApi, PropsWithChildren<Props>>(function (
    {media, autoPlay, className, children},
    ref
) {
    const [videoElement, setVideoElement] = useState<HTMLVideoElement | null>(null);

    useImperativeHandle(ref, () => ({
        DURATIONCHANGE_EVENT: "durationchange",
        CANPLAY_EVENT: "canplay",
        TIMECHANGE_EVENT: "timeupdate",
        PLAY_EVENT: "play",
        PAUSE_EVENT: "pause",

        play(): void {
            if (!videoElement) {
                return;
            }

            videoElement.play();
        },
        pause(): void {
            if (!videoElement) {
                return;
            }

            videoElement.pause();
        },
        isPaused(): boolean {
            if (!videoElement) {
                return true;
            }

            return videoElement.paused;
        },
        setPlaybackRate(value: number): void {
            if (!videoElement) {
                return;
            }

            videoElement.playbackRate = value;
        },
        getPlaybackRate(): number {
            if (!videoElement) {
                return 1;
            }

            return videoElement.playbackRate;
        },
        setVolume(value: number): void {
            if (!videoElement) {
                return;
            }

            videoElement.volume = value;
        },
        getVolume(): number {
            if (!videoElement) {
                return 0;
            }

            return videoElement.volume;
        },
        setCurrentTime(value: number): void {
            if (!videoElement) {
                return;
            }

            videoElement.currentTime = value;
        },
        getCurrentTime(): number {
            if (!videoElement) {
                return 0;
            }

            return videoElement.currentTime;
        },
        getDuration(): number {
            if (!videoElement) {
                return 0;
            }

            return videoElement.duration;
        },
        canPlay(): boolean {
            if (!videoElement) {
                return false;
            }

            return videoElement.readyState >= 1;
        },
        getAudioTracks(): MediaInfo[] {
            return [];
        },
        setCurrentAudioTrack(track: MediaInfo) {
        },
        getCurrentAudioTrack(): MediaInfo | null {
            return null;
        },
        addEventListener(event: string, listener: () => void): void {
            if (!videoElement) {
                return;
            }

            videoElement.addEventListener(event, listener);
        },
        removeEventListener(event: string, listener: () => void): void {
            if (!videoElement) {
                return;
            }

            videoElement.removeEventListener(event, listener);
        },
        debug(): any {
            return videoElement;
        },
        getVideoElement(): HTMLVideoElement | null {
            return videoElement;
        }
    }), [videoElement]);

    return (
        <video
            ref={setVideoElement}
            autoPlay={autoPlay}
            src={media.src}
            className={className}
            crossOrigin="anonymous"
            controls={false}
        >
            {children}
        </video>
    );
});

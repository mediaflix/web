import {Subtitle} from "../../../api/models/Subtitle";

interface Props {
    subtitles: (Subtitle | null)[],
    subtitle: Subtitle | null,
    setSubtitle: (subtitle: Subtitle | null) => void,
}

export function SubtitleSelection(
    {subtitles, subtitle, setSubtitle}: Props
) {
    return (
        <ul>
            {subtitles.map(track => (
                <li key={track?.src || "none"}>
                    <strong>{track?.language || "none"}</strong>
                    &nbsp;{track?.specifier}
                    &nbsp;{track?.format}
                    &nbsp;
                    <button
                        disabled={subtitle === track}
                        onClick={() => setSubtitle(track)}
                    >
                        Choose
                    </button>
                </li>
            ))}
        </ul>
    );
}

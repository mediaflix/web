import {forwardRef, Fragment, HTMLProps} from "react";
import {createPortal} from "react-dom";
import {usePlayerApi} from "./VideoContext";

export const Track = forwardRef<HTMLTrackElement, HTMLProps<HTMLTrackElement>>(function (
    props, ref
) {
    const playerApi = usePlayerApi();
    const videoElement = playerApi?.getVideoElement();
    if (videoElement) {
        return createPortal(
            <track ref={ref} {...props} />,
            videoElement
        );
    } else {
        return <Fragment/>;
    }
});

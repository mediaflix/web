import {createContext, useContext} from "react";
import {PlayerApi} from "./PlayerApi";

const playerContext = createContext<PlayerApi | null>(null);
export const PlayerProvider = playerContext.Provider;
export const usePlayerApi = () => useContext<PlayerApi | null>(playerContext);

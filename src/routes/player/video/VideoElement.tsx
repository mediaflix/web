import { forwardRef, PropsWithChildren } from "react";
import {Media} from "../../../api/models/Media";
import {DashVideoElement} from "./DashVideoElement";
import {RawVideoElement} from "./RawVideoElement";
import {PlayerApi} from "./PlayerApi";

interface Props {
    media: Media,
    autoPlay: boolean,
    className?: string,
}

export const VideoElement = forwardRef<PlayerApi, PropsWithChildren<Props>>(function (
    props: Props, ref
) {
    switch (props.media.mime) {
        case "application/dash+xml":
            return <DashVideoElement ref={ref} {...props} />;
        default:
            return <RawVideoElement ref={ref} {...props} />;
    }
});

declare module "imsc" {
    /*
 * Copyright (c) 2016, Pierre-Anthony Lemieux <pal@sandflow.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

    export interface TtmlDocument {
        getMediaTimeEvents(): number[];

        getMediaTimeRange(): number[];
    }

    export type ISD = unknown;
    export type ISDState = unknown;

    /**
     * Allows a client to provide callbacks to handle children of the <metadata> element
     */
    export interface MetadataHandler {
        /**
         * Called when the opening tag of an element node is encountered.
         * @param ns Namespace URI of the element
         * @param name Local name of the element
         * @param attributes List of attributes, each consisting of a `uri`, `name` and `value`
         */
        onOpenTag(ns: string, name: string, attributes: Attribute[]): void;

        /**
         * Called when the closing tag of an element node is encountered.
         */
        onCloseTag(): void;

        /**
         * Called when a text node is encountered.
         * @param contents Contents of the text node
         */
        onText(contents: string): void;
    }

    export interface Attribute {
        uri: string;
        name: string;
        value: string;
    }

    /**
     * Generic interface for handling events. The interface exposes four
     * methods:
     * * <pre>info</pre>: unusual event that does not result in an inconsistent state
     * * <pre>warn</pre>: unexpected event that should not result in an inconsistent state
     * * <pre>error</pre>: unexpected event that may result in an inconsistent state
     * * <pre>fatal</pre>: unexpected event that results in an inconsistent state
     *   and termination of processing
     * Each method takes a single <pre>string</pre> describing the event as argument,
     * and returns a single <pre>boolean</pre>, which terminates processing if <pre>true</pre>.
     */
    export interface ErrorHandler {
        info(error: string): boolean;

        warn(error: string): boolean;

        error(error: string): boolean;

        fatal(error: string): boolean;
    }

    /**
     * Parses an IMSC1 document into an opaque in-memory representation that exposes
     * a single method <pre>getMediaTimeEvents()</pre> that returns a list of time
     * offsets (in seconds) of the ISD, i.e. the points in time where the visual
     * representation of the document change. `metadataHandler` allows the caller to
     * be called back when nodes are present in <metadata> elements.
     */
    export function fromXML(
        xmlstring: string,
        errorHandler?: ErrorHandler,
        metadataHandler?: MetadataHandler,
    ): TtmlDocument | null;

    /**
     * Creates a canonical representation of an IMSC1 document returned by <pre>imscDoc.fromXML()</pre>
     * at a given absolute offset in seconds. This offset does not have to be one of the values returned
     * by <pre>getMediaTimeEvents()</pre>.
     */
    export function generateISD(
        tt: TtmlDocument,
        offset?: number,
        errorHandler?: ErrorHandler
    ): ISD;

    /**
     * Renders an ISD object (returned by <pre>generateISD()</pre>) into a
     * parent element, that must be attached to the DOM. The ISD will be rendered
     * into a child <pre>div</pre>
     * with heigh and width equal to the clientHeight and clientWidth of the element,
     * unless explicitly specified otherwise by the caller. Images URIs specified
     * by <pre>smpte:background</pre> attributes are mapped to image resource URLs
     * by an <pre>imgResolver</pre> function. The latter takes the value of <code>smpte:background</code>
     * attribute and an <code>img</code> DOM element as input, and is expected to
     * set the <code>src</code> attribute of the <code>img</code> to the absolute URI of the image.
     * <pre>displayForcedOnlyMode</pre> sets the (boolean)
     * value of the IMSC1 displayForcedOnlyMode parameter. The function returns
     * an opaque object that should passed in <code>previousISDState</code> when this function
     * is called for the next ISD, otherwise <code>previousISDState</code> should be set to
     * <code>null</code>.
     */
    export function renderHTML(
        isd: ISD,
        element: HTMLElement,
        /**
         * Function that maps <pre>smpte:background</pre> URIs to URLs resolving to image resource
         * @param {string} <pre>smpte:background</pre> URI
         */
        imgResolver?: (backgroundUri: string) => string,
        height?: number,
        width?: number,
        displayForcedOnlyMode?: boolean,
        errorHandler?: ErrorHandler,
        previousISDState?: ISDState,
        enableRollUp?: boolean,
    ): ISDState;
}


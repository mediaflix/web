import {createContext, useContext} from "react";
import {ContentMeta} from "../api/models/dto/ContentMeta";

const CurrentContentContext = createContext<ContentMeta | null>(null);
export const CurrentContentProvider = CurrentContentContext.Provider;
export const CurrentContentConsumer = CurrentContentContext.Consumer;
export const useCurrentContent = () => useContext(CurrentContentContext);

import {RequestErrorKind} from "./request/RequestErrorKind";
import {RequestError} from "./request/RequestError";

export async function fetchJson<T>(
    baseUrl: URL,
    path: string,
    options?: RequestInit
): Promise<T> {
    const url = new URL(path, baseUrl).toString();
    const response = await fetch(url, options);
    if (response.ok) {
        return await response.json();
    } else if (response.status in RequestErrorKind) {
        throw new RequestError(response.statusText, response.status)
    } else {
        throw new RequestError(response.statusText, RequestErrorKind.UnknownError)
    }
}

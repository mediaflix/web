import * as React from "react";
import ReactDOM from "react-dom";

export function HeadPortal({children}: React.PropsWithChildren<{}>) {
    return ReactDOM.createPortal(children, document.head);
}

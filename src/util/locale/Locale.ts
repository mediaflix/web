export interface Locale {
    language: string,
    region: string,
}

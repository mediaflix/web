import {createContext, useContext} from "react";
import {Locale} from "./Locale";

const LocalizedContext = createContext<Locale>({
    language: "en",
    region: "US",
});
export const LocaleProvider = LocalizedContext.Provider;
export const LocaleConsumer = LocalizedContext.Consumer;
export const useLocale = () => useContext(LocalizedContext);

import {RegionalData} from "./RegionalData";

export interface LocalizedData extends RegionalData {
    languages: string[],
    kind: string,
}

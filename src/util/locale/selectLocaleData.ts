import {Locale} from "./Locale";
import {LocalePriority} from "./LocalePriority";
import {LocalizedData} from "./LocalizedData";

function filterVersions<T extends LocalizedData>(
    locale: Locale,
    method: LocalePriority,
    data: T[]
): T | undefined {
    switch (method) {
        case LocalePriority.REGION:
            return data.sort((a, b) =>
                +(b.languages?.includes(locale.language) || false) -
                +(a.languages?.includes(locale.language) || false)
            ).find((el) =>
                el.region === locale.region
                || el.region === null);
        case LocalePriority.LOCALE:
            return data.sort((a, b) =>
                +(b.region === locale.region) -
                +(a.region === locale.region)
            ).find((el) =>
                el.languages?.includes(locale.language)
                || el.languages.length === 0);

    }
}

export function selectLocaleVersion<T extends LocalizedData>(
    locale: Locale,
    method: LocalePriority,
    data: T[]
): T | null {
    return filterVersions(locale, method, data.filter(it => it?.kind === undefined || it?.kind === "localized"))
        || data.find(it => it.kind === "primary")
        || data.find(it => it.kind === "original")
        || null;
}

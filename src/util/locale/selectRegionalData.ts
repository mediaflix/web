import {Locale} from "./Locale";
import {RegionalData} from "./RegionalData";

export function selectRegionalVersion<T extends RegionalData>(
    locale: Locale,
    data: T[]
): T | null {
    return data.find((el) => {
        return (el.region === locale.region);
    }) || null;
}

import {useCallback, useEffect, useState} from "react";
import {PlayerApi} from "../../routes/player/video/PlayerApi";
import {MediaInfo} from "dashjs";

export function useAudioTracks(video: PlayerApi | null):
    [MediaInfo[], MediaInfo | null, (track: MediaInfo) => void] {
    const [audioTracks, setAudioTracks] = useState<MediaInfo[]>([]);
    const [currentTrack, setCurrentTrack] = useState<MediaInfo | null>(null);

    const listener = useCallback(() => {
        window.requestAnimationFrame(() => {
            if (video !== null) {
                setAudioTracks(video.getAudioTracks());
                setCurrentTrack(video.getCurrentAudioTrack);
            }
        })
    }, [video]);

    const updateAudioTracks = useCallback((track: MediaInfo) => {
        video?.setCurrentAudioTrack(track);
        listener();
    }, [video, listener]);

    useEffect(() => {
        if (video?.canPlay()) {
            setAudioTracks(video.getAudioTracks());
            setCurrentTrack(video.getCurrentAudioTrack);
        } else {
            video?.addEventListener(video.CANPLAY_EVENT, listener)
            return () => {
                video?.removeEventListener(video.CANPLAY_EVENT, listener)
            }
        }
    }, [listener, video]);
    return [audioTracks, currentTrack, updateAudioTracks];
}

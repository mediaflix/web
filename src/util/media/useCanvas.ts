import {useLayoutEffect, useRef, useState} from "react";

export const useCanvas = (width: number | null, height: number | null): [HTMLCanvasElement, CanvasRenderingContext2D | null] => {
    const canvas = useRef(document.createElement("canvas"));
    const [context, setContext] = useState<CanvasRenderingContext2D | null>(null);

    useLayoutEffect(() => {
        if (width !== null && height !== null) {
            canvas.current.width = width;
            canvas.current.height = height;
            setContext(canvas.current.getContext("2d"));
        }
    }, [width, height, setContext]);

    return [canvas.current, context];
}

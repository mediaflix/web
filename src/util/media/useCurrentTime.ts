import {useEffect, useState} from "react";
import {PlayerApi} from "../../routes/player/video/PlayerApi";

export const useCurrentTime = (video: PlayerApi | null) => {
    const [position, setPosition] = useState<number>(0);
    useEffect(() => {
        if (video !== null) {
            const listener = () => {
                window.requestAnimationFrame(() => {
                    setPosition(video.getCurrentTime());
                })
            };
            video.addEventListener(video.TIMECHANGE_EVENT, listener)
            return () => {
                video.removeEventListener(video.TIMECHANGE_EVENT, listener)
            }
        }
    }, [video]);
    return position;
}

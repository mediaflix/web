import {useEffect} from "react";

function onlyInDebug<U, R>(callback: (...args: U[]) => R, defValue?: R): (...args: U[]) => R {
    if (process.env.NODE_ENV === "development") {
        return callback
    } else {
        return () => defValue as R;
    }
}

export const useDebugInfo = onlyInDebug((key: string, value: any) => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    useEffect(() => {
        // @ts-ignore
        if (!window.mediaDebug) {
            // @ts-ignore
            window.mediaDebug = {};
        }
        // @ts-ignore
        window.mediaDebug[key] = value;
    }, [key, value]);
});

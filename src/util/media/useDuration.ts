import {PlayerApi} from "../../routes/player/video/PlayerApi";
import {useVideoApi} from "./useVideoApi";
import {useCallback} from "react";

export const useDuration = (video: PlayerApi | null) => {
    const callback = useCallback((videoApi: PlayerApi) =>
        videoApi.getDuration(), []);
    return useVideoApi<number>(
        [video?.DURATIONCHANGE_EVENT],
        video,
        0,
        callback
    )
}

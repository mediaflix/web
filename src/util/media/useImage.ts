import {useEffect, useMemo, useState} from "react";

export const useImage = (url: string | null): HTMLImageElement | null => {
    const [imageData, setImageData] = useState<HTMLImageElement | null>(null);
    const image = useMemo(() => {
        if (url === null) {
            return null;
        } else {
            const image = new Image();
            image.setAttribute("crossorigin", "anonymous");
            image.src = url;
            return image;
        }
    }, [url]);
    useEffect(() => {
        let isCancelled = false;
        image?.decode().then(() => {
            if (!isCancelled) {
                setImageData(image);
            }
        }).catch(err => {
            console.error("Error while decoding image " + url + ": ", err);
        });
        return () => {
            isCancelled = true;
        }
    }, [image, url]);

    return imageData;
}

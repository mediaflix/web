import {PlayerApi} from "../../routes/player/video/PlayerApi";
import {useVideoApi} from "./useVideoApi";
import {useCallback} from "react";

export const usePaused = (video: PlayerApi | null) => {
    const callback = useCallback((videoApi: PlayerApi) =>
        videoApi.isPaused(), []);
    return useVideoApi<boolean>(
        [video?.PLAY_EVENT, video?.PAUSE_EVENT],
        video,
        true,
        callback
    );
}

import {PlayerApi} from "../../routes/player/video/PlayerApi";
import {useVideoApi} from "./useVideoApi";

interface PlayerState {
    currentTime: number,
    duration: number,
    paused: boolean,
}

export const usePlayerState = (video: PlayerApi | null) => useVideoApi<PlayerState>(
    [
        video?.DURATIONCHANGE_EVENT,
        video?.TIMECHANGE_EVENT,
        video?.PLAY_EVENT,
        video?.PAUSE_EVENT,
    ],
    video,
    {
        currentTime: 0,
        duration: 0,
        paused: true,
    },
    (videoApi: PlayerApi) => {
        return {
            currentTime: videoApi.getCurrentTime(),
            duration: videoApi.getDuration(),
            paused: videoApi.isPaused(),
        }
    }
)

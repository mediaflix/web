import {useEffect, useState} from "react";

export const useTextTrackCues = (track: HTMLTrackElement | null) => {
    const [cues, setCues] = useState<VTTCue[]>([]);
    useEffect(() => {
        if (track !== null) {
            track.track.mode = "hidden";
            if (track.readyState >= 1) {
                setCues(Array.from(track.track.cues || []) as VTTCue[])
            } else {
                let animationFrame: number | null = null;
                const listener = () => {
                    animationFrame = window.requestAnimationFrame(() => {
                        setCues(Array.from(track.track.cues || []) as VTTCue[])
                    })
                };
                track.addEventListener("load", listener)
                return () => {
                    track.removeEventListener("load", listener);
                    if (animationFrame) {
                        window.cancelAnimationFrame(animationFrame);
                    }
                }
            }
        }
    }, [track]);
    return cues;
}

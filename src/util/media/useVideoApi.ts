import {useEffect, useState} from "react";
import {PlayerApi} from "../../routes/player/video/PlayerApi";

export function useVideoApi<T>(
    events: (string|undefined|null)[],
    video: PlayerApi | null,
    initial: T,
    mapper: (video: PlayerApi) => T
): T {
    const [result, setResult] = useState<T>(initial);
    useEffect(() => {
        if (video !== null) {
            let requestFrame: number | null = null;
            const listener = () => {
                if (requestFrame !== null) {
                    window.cancelAnimationFrame(requestFrame)
                }
                requestFrame = window.requestAnimationFrame(() => {
                    setResult(mapper(video));
                })
            };
            listener();
            for (let event in events) {
                if (event !== null && event !== undefined) {
                    video.addEventListener(event, listener);
                }
            }
            return () => {
                if (requestFrame) {
                    window.cancelAnimationFrame(requestFrame);
                }
                for (let event in events) {
                    if (event !== null && event !== undefined) {
                        video.removeEventListener(event, listener);
                    }
                }
            }
        }
    }, [events, mapper, video]);
    return result;
}

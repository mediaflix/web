export enum PlayabilityRating {
    UNLIKELY = 0,
    MAYBE = 1,
    PROBABLY = 2,
}

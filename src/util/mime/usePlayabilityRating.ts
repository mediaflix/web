import {useRef} from "react";
import {Media} from "../../api/models/Media";
import {sortNumericallyDesc} from "../sort/sortNumerically";
import {PlayabilityRating} from "./PlayabilityRating";
import {videoMimeString} from "./videoMimeString";

export const usePlayabilityRating = () => {
    const mediaElement = useRef(document.createElement("video")).current;
    return (media: Media) => {
        switch (mediaElement.canPlayType(videoMimeString(media))) {
            case "":
                return PlayabilityRating.UNLIKELY;
            case "maybe":
                return PlayabilityRating.MAYBE;
            case "probably":
                return PlayabilityRating.PROBABLY;
        }
    }
}

export const selectPlayabileMedia = (
    playabilityRating: (media: Media) => PlayabilityRating,
    media: Media[]
): Media | null =>
    media.sort(sortNumericallyDesc(it => playabilityRating(it)))[0]
    || null

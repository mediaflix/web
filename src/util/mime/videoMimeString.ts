import {Media} from "../../api/models/Media";

export const videoMimeString = (media: Media) =>
    `${media.mime}; codecs="${media.codecs.join(", ")}"`;

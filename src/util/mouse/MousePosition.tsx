export interface MousePosition {
    absolute: number,
    relative: number
}

import {MouseEvent} from "react";
import {MousePosition} from "./MousePosition";

export function getMousePosition(event: MouseEvent<HTMLDivElement>): MousePosition | null {
    const position = event.clientX - event.currentTarget.offsetLeft;
    const width = event.currentTarget.offsetWidth;
    if (position < 0) return null;
    if (position > width) return null;
    return {
        absolute: event.clientX,
        relative: position / width
    };
}

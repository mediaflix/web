import {useEffect, useState} from "react";
import ResizeObserver from 'resize-observer-polyfill';
import {BoundingRect} from "./boundingRect";

export const useBoundingRect = <T extends HTMLElement>(it: T | null) => {
    const [rect, setRect] = useState<BoundingRect | null>(null);
    useEffect(() => {
        if (it !== null) {
            let animationFrame: number | null = null;
            const listener = () => {
            };
            listener();
            const observer = new ResizeObserver(([element]) => {
                animationFrame = window.requestAnimationFrame(() => {
                    animationFrame = null;
                    setRect(it.getBoundingClientRect() || null);
                });
            })
            observer.observe(it);
            return () => {
                observer.unobserve(it);
                if (animationFrame !== null) {
                    window.cancelAnimationFrame(animationFrame);
                }
            }
        }
    }, [it]);
    return rect;
}

import {useBoundingRect} from "./useBoundingRect";

export const useOffsetAbsolute = <T extends HTMLElement, U extends HTMLElement>(parent: T | null, child: U | null, position: number) => {
    const parentRect = useBoundingRect(parent);
    const childRect = useBoundingRect(child);

    if (parentRect === null || childRect === null) {
        return 0;
    }

    const offset = position - parentRect.left - childRect.width / 2;
    const maximum = parentRect.width - childRect.width;
    return Math.max(0, Math.min(offset, maximum));
}

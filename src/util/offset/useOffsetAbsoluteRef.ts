import {useState} from "react";
import {useOffsetAbsolute} from "./useOffsetAbsolute";

export const useOffsetAbsoluteRef = <T extends HTMLElement, U extends HTMLElement>(position: number):
    [(it: T | null) => void, (it: U | null) => void, number] => {
    const [parent, setParent] = useState<T | null>(null);
    const [child, setChild] = useState<U | null>(null);

    return [setParent, setChild, useOffsetAbsolute(parent, child, position)];
}

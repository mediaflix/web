export function parseUrl(url: string | null | undefined): URL | null {
    if (url === null || url === undefined || url === "") {
        return null;
    }

    try {
        return new URL(url);
    } catch (_) {
        return null;
    }
}

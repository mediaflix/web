import {RequestError} from "./RequestError";
import {RequestErrorKind} from "./RequestErrorKind";

export class RequestClient {
    private baseUrl: string

    constructor(baseUrl: string) {
        this.baseUrl = baseUrl;
    }

    protected async request<T>(path: string, headers?: RequestInit): Promise<T> {
        const url = new URL(path, this.baseUrl).toString();
        const response = await fetch(url, headers);
        if (response.ok) {
            return await response.json();
        } else {
            if (response.status in RequestErrorKind) {
                throw new RequestError(response.statusText, response.status)
            } else {
                throw new RequestError(response.statusText, RequestErrorKind.UnknownError)
            }
        }
    }
}


import {RequestErrorKind} from "./RequestErrorKind";

export class RequestError extends Error {
    public kind: RequestErrorKind;
    public message: string;

    constructor(message: string, kind: RequestErrorKind) {
        super(`RequestError ${kind}: ${message}`);
        this.kind = kind;
        this.message = message;
    }
}

export const sortLexicallyAsc = <T>(accessor: ((data: T) => string) = String):
    (a: T, b: T) => number => (a: T, b: T) => accessor(a).localeCompare(accessor(b));

export const sortLexicallyDesc = <T>(accessor: ((data: T) => string) = String):
    (a: T, b: T) => number => (a: T, b: T) => accessor(b).localeCompare(accessor(a));

export const sortNumericallyAsc = <T>(accessor: ((data: T) => number) = Number):
    (a: T, b: T) => number => (a: T, b: T) => accessor(a) - accessor(b);

export const sortNumericallyDesc = <T>(accessor: ((data: T) => number) = Number):
    (a: T, b: T) => number => (a: T, b: T) => accessor(b) - accessor(a);

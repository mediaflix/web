export interface ImageSprite {
    src: string,
    fragment: {
        x: number,
        y: number,
        w: number,
        h: number,
    }
}

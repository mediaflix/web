import parse from "../media/MediaFragment";
import {ImageSprite} from "./ImageSprite";

export function parseImageSprite(src: string | null): ImageSprite | null {
    if (src === null) {
        return null;
    }
    const fragment = parse(src)["xywh"] || undefined;
    if (fragment?.x !== undefined
        && fragment?.y !== undefined
        && fragment?.w !== undefined
        && fragment?.h !== undefined) {
        const url = new URL(src);
        url.hash = "";
        return {
            src: url.toString(),
            fragment: {
                x: fragment.x,
                y: fragment.y,
                w: fragment.w,
                h: fragment.h,
            }
        }
    } else {
        return null;
    }
}

import {useEffect, useState} from "react";
import {ImageSprite} from "./ImageSprite";
import {useCanvas} from "../media/useCanvas";

export const useImageSprite = (imageSprite: ImageSprite | null, image: HTMLImageElement | null): string | null => {
    const [canvas, context] = useCanvas(
        imageSprite?.fragment?.w || null,
        imageSprite?.fragment?.h || null
    );

    const [data, setData] = useState<string | null>(null);
    useEffect(() => {
        if (canvas && context && image && imageSprite?.fragment) {
            const {x, y, w, h} = imageSprite.fragment;
            context.drawImage(image, x, y, w, h, 0, 0, w, h);
            setData(canvas.toDataURL("image/png"));
        } else if (imageSprite?.fragment === null) {
            setData(imageSprite.src);
        }
    }, [canvas, context, image, imageSprite]);

    return data;
}

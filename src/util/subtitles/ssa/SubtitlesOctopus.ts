import {browserSupportsWebAssembly} from "./browserSupportsWebAssembly";
import browserHasAlphaBug from "./browserHasAlphaBug";
import {SubtitlesOctopusOptions} from "./SubtitlesOctopusOptions";
import ResizeObserver from "resize-observer-polyfill";

export default class SubtitlesOctopus {
    /**
     * HTML canvas element (optional if video specified)
     */
    public canvas: HTMLCanvasElement
    /**
     * Speedup render for heavy subs
     */
    public lossyRender: boolean
    /**
     * HTML video element (optional if canvas specified)
     */
    public video?: HTMLVideoElement
    /**
     * Array with links to fonts used in sub (optional)
     */
    public fonts: string[]
    /**
     * Object with all available fonts (optional).
     * Key is font name in lower case, value is link: {"arial": "/font1.ttf"}
     */
    public availableFonts: {
        [key: string]: string
    }
    /**
     * Function called when SubtitlesOctopus is ready (optional)
     */
    public onReadyEvent?: () => void
    /**
     * Link to WebAssembly worker
     */
    public workerUrl: string
    /**
     * Link to sub file (optional if subContent specified)
     */
    public subUrl?: string
    /**
     * Sub content (optional if subUrl specified)
     */
    public subContent: string | null
    /**
     * Function called in case of critical error meaning sub wouldn't be shown
     * and you should use alternative method (for instance it occurs if browser
     * doesn't support web workers).
     */
    public onErrorEvent?: (event: any) => void
    /**
     * When debug enabled, some performance info printed in console.
     */
    public debug: boolean
    /**
     * Time offset would be applied to currentTime from video (option)
     */
    public timeOffset: number

    private isOurCanvas: boolean
    private canvasParent: HTMLElement | null
    private lastRenderTime: number = 0
    private pixelRatio: number
    private readonly hasAlphaBug: boolean
    private frameId = 0
    private workerActive = false
    private readonly worker: Worker
    private ctx: CanvasRenderingContext2D
    private readonly bufferCanvas: HTMLCanvasElement
    private bufferCanvasCtx: CanvasRenderingContext2D
    private ro: ResizeObserver
    private renderStart: number = 0;

    private renderFramesData: {
        bitmaps: {
            w: number,
            h: number,
            x: number,
            y: number,
            bitmap: ImageBitmap
        }[],
        canvases: {
            w: number,
            h: number,
            x: number,
            y: number,
            buffer: ArrayBuffer
        }[],
        spentTime: number,
        libassTime: number,
        decodeTime: number
    } | null = null

    constructor(
        options: SubtitlesOctopusOptions
    ) {
        this.onWorkerMessage = this.onWorkerMessage.bind(this);
        this.onWorkerError = this.onWorkerError.bind(this);
        this.resize = this.resize.bind(this);
        this.resizeWithTimeout = this.resizeWithTimeout.bind(this);
        this.renderFrames = this.renderFrames.bind(this);
        this.renderFastFrames = this.renderFastFrames.bind(this);

        const supportsWebAssembly = browserSupportsWebAssembly();
        console.log("WebAssembly support detected: " + (supportsWebAssembly ? "yes" : "no"));

        this.canvas = options.canvas; // HTML canvas element (optional if video specified)
        this.lossyRender = options.lossyRender || false; // Speedup render for heavy subs
        this.isOurCanvas = false; // (internal) we created canvas and manage it
        this.video = options.video; // HTML video element (optional if canvas specified)
        this.canvasParent = null; // (internal) HTML canvas parent element
        this.fonts = options.fonts || []; // Array with links to fonts used in sub (optional)
        this.availableFonts = options.availableFonts || {}; // Object with all available fonts (optional). Key is font name in lower case, value is link: {"arial": "/font1.ttf"}
        this.onReadyEvent = options.onReady; // Function called when SubtitlesOctopus is ready (optional)
        if (supportsWebAssembly) {
            this.workerUrl = options.workerUrl || 'subtitles-octopus-worker.js'; // Link to WebAssembly worker
        } else {
            this.workerUrl = options.legacyWorkerUrl || 'subtitles-octopus-worker-legacy.js'; // Link to legacy worker
        }
        this.subUrl = options.subUrl; // Link to sub file (optional if subContent specified)
        this.subContent = options.subContent || null; // Sub content (optional if subUrl specified)
        this.onErrorEvent = options.onError; // Function called in case of critical error meaning sub wouldn't be shown and you should use alternative method (for instance it occurs if browser doesn't support web workers).
        this.debug = options.debug || false; // When debug enabled, some performance info printed in console.
        this.lastRenderTime = 0; // (internal) Last time we got some frame from worker
        this.pixelRatio = window.devicePixelRatio || 1; // (internal) Device pixel ratio (for high dpi devices)

        this.timeOffset = options.timeOffset || 0; // Time offset would be applied to currentTime from video (option)

        this.hasAlphaBug = false;

        if (!window.Worker) {
            this.onWorkerError('worker not supported');
        }
        // Worker
        this.worker = new Worker(this.workerUrl);
        this.worker.onmessage = this.onWorkerMessage;
        this.worker.onerror = this.onWorkerError;
        this.workerActive = false;
        const ctx = this.canvas.getContext('2d');
        if (ctx === null) {
            throw new Error("Could not create 2D canvas content");
        }
        this.ctx = ctx;
        this.bufferCanvas = document.createElement('canvas');
        const bufferCanvasCtx = this.bufferCanvas.getContext('2d')
        if (bufferCanvasCtx === null) {
            throw new Error("Could not create 2D canvas content");
        }
        this.bufferCanvasCtx = bufferCanvasCtx;
        // Support Element Resize Observer
        this.ro = new ResizeObserver(this.resizeWithTimeout);

        this.hasAlphaBug = browserHasAlphaBug();
        if (this.hasAlphaBug) {
            console.log("Detected a browser having issue with transparent pixels, applying workaround");
        }
        this.setVideo(options.video);
        this.setSubUrl(options.subUrl);
        this.worker.postMessage({
            target: 'worker-init',
            width: this.canvas.width,
            height: this.canvas.height,
            URL: document.URL,
            currentScript: this.workerUrl,
            preMain: true,
            fastRender: this.lossyRender,
            subUrl: this.subUrl,
            subContent: this.subContent,
            fonts: this.fonts,
            availableFonts: this.availableFonts,
            debug: this.debug
        });
    }

    private renderFrames() {
        const data = this.renderFramesData;
        if (data) {
            const beforeDrawTime = performance.now();
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            for (let i = 0; i < data.canvases.length; i++) {
                const image = data.canvases[i];
                this.bufferCanvas.width = image.w;
                this.bufferCanvas.height = image.h;
                const imageBuffer = new Uint8ClampedArray(image.buffer);
                if (this.hasAlphaBug) {
                    for (let j = 3; j < imageBuffer.length; j = j + 4) {
                        imageBuffer[j] = (imageBuffer[j] >= 1) ? imageBuffer[j] : 1;
                    }
                }
                const imageData = new ImageData(imageBuffer, image.w, image.h);
                this.bufferCanvasCtx.putImageData(imageData, 0, 0);
                this.ctx.drawImage(this.bufferCanvas, image.x, image.y);
            }
            if (this.debug) {
                const drawTime = Math.round(performance.now() - beforeDrawTime);
                console.log(Math.round(data.spentTime) + ' ms (+ ' + drawTime + ' ms draw)');
                this.renderStart = performance.now();
            }
        }
    }

    private renderFastFrames() {
        const data = this.renderFramesData;
        if (data) {
            const beforeDrawTime = performance.now();
            this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            for (let i = 0; i < data.bitmaps.length; i++) {
                const image = data.bitmaps[i];
                this.ctx.drawImage(image.bitmap, image.x, image.y);
            }
            if (this.debug) {
                const drawTime = Math.round(performance.now() - beforeDrawTime);
                console.log(data.bitmaps.length + ' bitmaps, libass: ' + Math.round(data.libassTime) + 'ms, decode: ' + Math.round(data.decodeTime) + 'ms, draw: ' + drawTime + 'ms');
                this.renderStart = performance.now();
            }
        }
    }

    private onWorkerError(error: any) {
        console.error('Worker error: ', error);
        if (this.onErrorEvent) {
            this.onErrorEvent(error);
        }
        if (!this.debug) {
            this.dispose();
            throw new Error('Worker error: ' + error);
        }
    };

    private setVideo(video: HTMLVideoElement | undefined) {
        this.video = video;
        if (video) {
            const timeupdate = () => this.setCurrentTime(video.currentTime + this.timeOffset)
            video.addEventListener("timeupdate", timeupdate, false);
            video.addEventListener("playing", () => {
                this.setIsPaused(false, video.currentTime + this.timeOffset);
            }, false);
            video.addEventListener("pause", () => {
                this.setIsPaused(true, video.currentTime + this.timeOffset);
            }, false);
            video.addEventListener(
                "seeking",
                () => video.removeEventListener("timeupdate", timeupdate),
                false
            );
            video.addEventListener(
                "seeked",
                () => {
                    video.addEventListener("timeupdate", timeupdate, false);
                    this.setCurrentTime(video.currentTime + this.timeOffset);
                },
                false
            );
            video.addEventListener(
                "ratechange",
                () => this.setRate(video.playbackRate),
                false
            );
            video.addEventListener(
                "timeupdate",
                () => this.setCurrentTime(video.currentTime + this.timeOffset),
                false
            );
            video.addEventListener(
                "waiting",
                () => this.setIsPaused(true, video.currentTime + this.timeOffset),
                false
            );
            window.addEventListener("resize", this.resize);
            this.ro.observe(video);

            if (video.videoWidth > 0) {
                this.resize();
            } else {
                const listener = () => {
                    video.removeEventListener("loadedmetadata", listener);
                    this.resize();
                }
                video.addEventListener("loadedmetadata", listener, false);
            }
        }
    }

    private setSubUrl(subUrl: string | undefined) {
        console.debug(`Loading Advanced SSA track: ${subUrl}`);
        this.subUrl = subUrl;
    }

    private onWorkerMessage(event: MessageEvent) {
        if (!this.workerActive) {
            this.workerActive = true;
            if (this.onReadyEvent) {
                this.onReadyEvent();
            }
        }
        const data = event.data;
        switch (data.target) {
            case 'stdout': {
                console.log(data.content);
                break;
            }
            case 'console-log': {
                console.log.apply(console, JSON.parse(data.content));
                break;
            }
            case 'console-debug': {
                console.debug.apply(console, JSON.parse(data.content));
                break;
            }
            case 'console-info': {
                console.info.apply(console, JSON.parse(data.content));
                break;
            }
            case 'console-warn': {
                console.warn.apply(console, JSON.parse(data.content));
                break;
            }
            case 'console-error': {
                console.error.apply(console, JSON.parse(data.content));
                break;
            }
            case 'stderr': {
                console.error(data.content);
                break;
            }
            case 'window': {
                // @ts-ignore
                window[data.method]();
                break;
            }
            case 'canvas': {
                switch (data.op) {
                    case 'getContext': {
                        const ctx = this.canvas.getContext(data.type, data.attributes);
                        if (ctx === null) {
                            throw new Error("Could not get canvas context");
                        }
                        this.ctx = ctx;
                        break;
                    }
                    case 'resize': {
                        this.resize();
                        break;
                    }
                    case 'renderCanvas': {
                        if (this.lastRenderTime === undefined || this.lastRenderTime < data.time) {
                            this.lastRenderTime = data.time;
                            this.renderFramesData = data;
                            window.requestAnimationFrame(this.renderFrames);
                        }
                        break;
                    }
                    case 'renderFastCanvas': {
                        if (this.lastRenderTime === undefined || this.lastRenderTime < data.time) {
                            this.lastRenderTime = data.time;
                            this.renderFramesData = data;
                            window.requestAnimationFrame(this.renderFastFrames);
                        }
                        break;
                    }
                    case 'setObjectProperty': {
                        // @ts-ignore
                        this.canvas[data.object][data.property] = data.value;
                        break;
                    }
                    default:
                        throw new Error("Unexpected worker message: unknown canvas operation");
                }
                break;
            }
            case 'tick': {
                this.frameId = data.id;
                this.worker.postMessage({
                    target: 'tock',
                    id: this.frameId
                });
                break;
            }
            case 'custom': {
                this.onCustomMessage(event);
                break;
            }
            case 'setimmediate': {
                this.worker.postMessage({
                    target: 'setimmediate'
                });
                break;
            }
            case 'get-events': {
                console.log(data.target);
                console.log(data.events);
                break;
            }
            case 'get-styles': {
                console.log(data.target);
                console.log(data.styles);
                break;
            }
            default:
                throw new Error("Unexpected worker message");
        }
    };

    private resize() {
        this.canvas.width = Math.floor(this.canvas.offsetWidth * window.devicePixelRatio);
        this.canvas.height = Math.floor(this.canvas.offsetHeight * window.devicePixelRatio);
        this.worker.postMessage({
            target: 'canvas',
            width: this.canvas.width,
            height: this.canvas.height
        });
    }

    private resizeWithTimeout() {
        this.resize();
        setTimeout(this.resize, 100);
    }

    private runBenchmark() {
        this.worker.postMessage({
            target: 'runBenchmark'
        });
    }

    private onCustomMessage(data: any, options?: any) {
        this.worker.postMessage({
            target: 'custom',
            userData: data,
            preMain: options?.preMain
        });
    }

    public setCurrentTime(currentTime: number) {
        this.worker.postMessage({
            target: 'video',
            currentTime: currentTime
        });
    }

    private setTrackByUrl(url: string) {
        this.worker.postMessage({
            target: 'set-track-by-url',
            url: url
        });
    }

    private setTrack(content: string) {
        this.worker.postMessage({
            target: 'set-track',
            content: content
        });
    }

    private freeTrack() {
        this.worker.postMessage({
            target: 'free-track'
        });
    }

    private setIsPaused(isPaused: boolean, currentTime: number) {
        this.worker.postMessage({
            target: 'video',
            isPaused: isPaused,
            currentTime: currentTime
        });
    }

    private setRate(rate: number) {
        this.worker.postMessage({
            target: 'video',
            rate: rate
        });
    }

    public dispose() {
        this.worker.postMessage({
            target: 'destroy'
        });

        window.removeEventListener("resize", this.resize);
        this.ro.disconnect();
        this.worker.terminate();
        this.workerActive = false;
    }

    private createEvent(event: any) {
        this.worker.postMessage({
            target: 'create-event',
            event: event
        });
    }

    private getEvents() {
        this.worker.postMessage({
            target: 'get-events'
        });
    }

    private setEvent(event: any, index: number) {
        this.worker.postMessage({
            target: 'set-event',
            event: event,
            index: index
        });
    }

    private removeEvent(index: number) {
        this.worker.postMessage({
            target: 'remove-event',
            index: index
        });
    }

    private createStyle(style: any) {
        this.worker.postMessage({
            target: 'create-style',
            style: style
        });
    }

    private getStyles() {
        this.worker.postMessage({
            target: 'get-styles'
        });
    }

    private setStyle(style: any, index: number) {
        this.worker.postMessage({
            target: 'set-style',
            style: style,
            index: index
        });
    }

    private removeStyle(index: number) {
        this.worker.postMessage({
            target: 'remove-style',
            index: index
        });
    }
}

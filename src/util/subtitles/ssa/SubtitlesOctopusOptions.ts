export interface SubtitlesOctopusOptions {
    /**
     * HTML canvas element (optional if video specified)
     */
    canvas: HTMLCanvasElement
    /**
     * Speedup render for heavy subs
     */
    lossyRender?: boolean
    /**
     * HTML video element (optional if canvas specified)
     */
    video?: HTMLVideoElement
    /**
     * Array with links to fonts used in sub (optional)
     */
    fonts?: string[]
    /**
     * Object with all available fonts (optional).
     * Key is font name in lower case, value is link: {"arial": "/font1.ttf"}
     */
    availableFonts?: {
        [key: string]: string
    }
    /**
     * Function called when SubtitlesOctopus is ready (optional)
     */
    onReady?: () => void,
    /**
     * Link to WebAssembly worker
     */
    workerUrl?: string
    /**
     * Link to legacy worker
     */
    legacyWorkerUrl?: string
    /**
     * Link to sub file (optional if subContent specified)
     */
    subUrl?: string
    /**
     * Sub content (optional if subUrl specified)
     */
    subContent?: string
    /**
     * Function called in case of critical error meaning sub wouldn't be shown
     * and you should use alternative method (for instance it occurs if browser
     * doesn't support web workers).
     */
    onError?: (error: any) => void
    /**
     * When debug enabled, some performance info printed in console.
     */
    debug?: boolean
    /**
     * Time offset would be applied to currentTime from video (option)
     */
    timeOffset?: number
}

/**
 * test for alpha bug, where e.g. WebKit can render a transparent pixel
 * (with alpha == 0) as non-black which then leads to visual artifacts
 */
export default function browserHasAlphaBug() {
    const canvas = document.createElement('canvas');
    const context = canvas.getContext('2d');

    const bufferCanvas = document.createElement('canvas');
    const bufferContext = bufferCanvas.getContext('2d');

    if (bufferContext === null || context === null) {
        throw new Error("Could not create canvas context");
    }

    bufferCanvas.width = 1;
    bufferCanvas.height = 1;
    const testBuf = new Uint8ClampedArray([0, 255, 0, 0]);
    const testImage = new ImageData(testBuf, 1, 1);
    bufferContext.clearRect(0, 0, 1, 1);
    context.clearRect(0, 0, 1, 1);
    const prePut = context.getImageData(0, 0, 1, 1).data;
    bufferContext.putImageData(testImage, 0, 0);
    context.drawImage(bufferCanvas, 0, 0);
    const postPut = context.getImageData(0, 0, 1, 1).data;
    return prePut[1] !== postPut[1];
}
